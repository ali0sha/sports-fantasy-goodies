// ==UserScript==
// @name         SportsFantasyNBATransfer
// @namespace    http://tampermonkey.net/
// @version      0.0.1
// @description  try to take over the world!
// @author       Alexey Seklenkov
// @match        https://www.sports.ru/fantasy/basketball/team/transfers/*.html
// @grant        none
// ==/UserScript==
/* jshint -W097 */
'use strict';

var player_url_tamplate = 'https://www.sports.ru/fantasy/basketball/player/info/150/(player_id).html';
var team_url_tamplate = 'https://www.sports.ru/fantasy/basketball/team/[team_id].html'

var week_games_cache = [];

function player_stat(ivn, spo){
    this.ivn = ivn;
    this.spo = spo;
}

function show_player_form(player_id, points, count){
    var $player_tr = $(`.transfer-table > tbody > tr[data-id="${player_id}"]`);
    var tmp_result = '';
    for(var i = 0; i < count; i++){
        tmp_result = tmp_result + points[i] + ', ';
    }
    $player_tr.find('.alx_spo_td').text(tmp_result);
}

function show_player_week_games(player_id, week_games, current_week, count){
    var $player_tr = $(`.transfer-table > tbody > tr[data-id="${player_id}"]`);
    var tmp_result = '';
    //console.log(current_week);
    for(let i = current_week; i < current_week + count; i++){
        //console.log(week_games[i]);
        tmp_result = tmp_result + week_games[i] + ', ';
    }
    $player_tr.find('.alx_ivn_td').text(tmp_result);
}

function add_cells(){
    $(".transfer-table > colgroup").empty()
    .append('<col width="63">')
    .append('<col width>')
    .append('<col width="70">')
    .append('<col width="70">')
    .append('<col width="40">')
    .append('<col width="40">');
    $(".transfer-table > thead > tr").each(function() {
        $(this).children().eq(1).after('<td class="sortable" data-field="week_games" data-type="number">ИВН</td>');
        $(this).children().eq(2).after('<td class="sortable" data-field="last_games_sum" data-type="number">Форма</td>');
    });
    $(".transfer-table > tbody > tr").each(function() {
        $(this).children().eq(1).after('<td class="alx_ivn_td">...</td>');
        $(this).children().eq(2).after('<td class="alx_spo_td">...</td>');
    });
}

async function add_values(current_week){
    $(".transfer-table > tbody > tr:not([alx_added])").each(async function() {
        let player_id = $(this).attr('data-id');
        let player_html = '';
        if ($(this).css('display') !== 'none'){
            player_html = await get_player_html(player_id);
            show_player_form(player_id, parse_points(player_html), 3);
            show_player_week_games(player_id, parse_weeks(player_html), current_week, 3);
            $(this).attr('alx_added', true);
        }
    });
}

function get_player_stat(player_id){
    return player_id;
}

function parse_points(player_html){
    var $player_html = $('<parse/>').append(player_html);
    var points = [];
    console.log($player_html.find('h1.titleH1 > b').text());
    $player_html.find('#stat > .stat > .stat-table > tbody > tr').each(function() {
        points.push(parseInt($(this).find('td:eq(18)').text()));
    });
    return points;
}

function parse_weeks(player_html){
    let weeks = [];
    let week_games = [];
    let $player_html = $('<parse/>').append(player_html);
    let player_team = $player_html.find('.info-block > .short-info > .profile-table > tbody > tr > td > a').text();
    if(typeof week_games_cache[player_team] === 'undefined'){
        $player_html.find('#calendar > .stat > .stat-table > tbody > tr').each(function() {
            var week = parseInt($(this).find('td:eq(0)').text().split("|неделя ")[1]);
            weeks.push(week);
        });
        weeks.forEach(function(x){
            week_games[x] = (typeof week_games[x]==='undefined') ? 1 : week_games[x]+1;;
        });
        week_games_cache[player_team] = week_games;
        console.log('запрос');
    }
    else {
        week_games = week_games_cache[player_team];
        console.log('кэш');
    }
    return week_games;
}


async function get_player_html(player_id){
    let player_html = await $.get(player_url_tamplate.replace('(player_id)', player_id)).promise();
    return player_html;
}

async function get_current_week(team_id){
    let team_html = await $.get(team_url_tamplate.replace('[team_id]', team_id)).promise();
    let $team_html = $($.parseHTML(team_html));
    let current_week = $team_html.find('.overBox > .team-info-block > .profile-table:eq(0) > tbody > tr:eq(0) > td:eq(0)').text();
    //return current_week.split('неделя ')[1];
    return parseInt(current_week.split('неделя ')[1]);
}

function get_team_id(){
    let team_url = window.location.pathname;
    let re = new RegExp("([0-9]*).html");
    return re.exec(team_url)[1];
}

/////////////////////////////////////////////////RUUUN////////////////////////////////////////////////////////
$(window).load(async function(){
    var current_week = await get_current_week(get_team_id());
    add_cells();
    add_values(current_week);
    $('.previous, .next, .sortable, .clubs-select, .amplua-select').on('click', function(){
        setTimeout(add_values, 1000, current_week);
    });
});
