// ==UserScript==
// @name         SportsFantasyLeagueNBAextJSON
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.sports.ru/fantasy/basketball/league/*.html*
// @grant        none
// ==/UserScript==

var html_field_div = `
<form class="overBox" action="">
   <div class="field-container">
      <div class="basket basket-field"></div>
   </div>
</form>
`;

var html_player = `
<ins class="player hold" data-amplua="[data-amplua]" style="top:[top]px; left:[left]px; height: 100px; width: 100px; cursor: default;"><img class="t-shirt" src=[t-shirt-url] alt=[club] title=[club]>
<span class="name">[player-name]</span>
<span class="pl-descr"><!--i class="ico info2" data-id="[data-id]"></i--><i class="ico point">[points]</i></span>	</ins>`;

var css_profile_table = `
.sfg-profile-table TH, .sfg-profile-table TD{
margin: 0 0 16px;
font-size: 13px;
border-collapse: collapse;
background: white;
border-top: 0px;
border-bottom: 0px;
vertical-align: middle;
line-height: 15px;
padding: 1px 0;
vertical-align: top;
text-align: left;
padding: 0 0 0 20px;
}
.team-info-block {
padding-top: 20px;
}
.overBox{
background-color: white;
padding-top: 10px;
padding-bottom: 10px;
line-height: 10px;
}
`

var player_points = [
    new Point(156, 208), //PG-1
    new Point(156, 530), //PG-2
    new Point(80,  125), //SG-1
    new Point(80,  447), //SG-2
    new Point(272, 154), //SF-1
    new Point(272, 476), //SF-2
    new Point(50,  20),  //PF-1
    new Point(50,  342), //PF-2
    new Point(222, 20),  //C-1
    new Point(222, 342)  //C-2
];

var teams = [];
var team_ids = [];
var json_ids = [];

var team_url = "https://www.sports.ru/fantasy/basketball/team/[team_id].html";
var team_json_url = 'https://www.sports.ru/fantasy/basketball/team/points/[team_id]/0.json';

(function() {
    'use strict';
    $('head').append(`<style type="text/css">${css_profile_table}</style>`);
    add_fields();
})();

function Point(x, y) {
  this.x = x;
  this.y = y;
}

function add_fields(){
    var team_url = '';
    var team_name = '';
    var team_id = '';
    var new_tr = '';
    var new_td = '';
    var field_div = '';
    $(".players-rank > table > colgroup").prepend($('<col width="25">'));
    $(".players-rank > table > thead > tr").prepend($('<td/>'));
    $(".players-rank > table > tbody > tr").each(function() {
        //собираем название команд и ссылки в массивы
        team_url = $(this).children('td').eq(2).children('a').attr('href'); //ссылка на команду
        team_name = $(this).children('td').eq(2).children('a').text(); //название команды
        var re = new RegExp("([0-9]*).html");
        team_id = re.exec(team_url)[1];
        teams.push(team_name);
        team_ids.push(team_id);

        //добавляем футбольное поле
        $new_tr = $(`<tr class="team_info" team_id = '${team_id}'></tr>`);
        $new_td = $('<td colspan="7"></td>');
        $new_td.append(get_field(team_id, team_name));
        $new_tr.append($new_td);
        $new_tr.insertAfter($(this));
        $new_tr.toggle();

        //добавляем кнопку для сворачивания
        $(this).prepend($('<td/>').append(get_toggele_button(team_id)));
    });
}

    function get_toggele_button(team_id){
        var $toggle_button = $('<i></i>');
        $toggle_button.addClass('toggle_button');
        $toggle_button.addClass('sign');
        $toggle_button.addClass('s-plus-gray');
        $toggle_button.attr('team_id', team_id);
        $toggle_button.click(function(){
            $(`.team_info[team_id = '${$(this).attr("team_id")}']`).toggle();
            if($(this).hasClass('s-minus-gray')) {
                $(this).removeClass('s-minus-gray').addClass('s-plus-gray');
            }
            else {
                $(this).removeClass('s-plus-gray').addClass('s-minus-gray');
            }
            if(get_team_loaded(team_id) !== "true") {
                add_players(team_id);
                get_team_html(team_id);
            }
        });
        return $toggle_button;
    }

    function get_field(team_id, team_name) {
        field_div = $(html_field_div);
        $(field_div).attr("team", team_name);
        $(field_div).attr("team_id", team_id);
        return field_div;
    }

    function set_team_loaded(team_id, loaded){
        $(`.toggle_button[team_id = '${team_id}']`).attr("loaded", loaded);
    }

    function get_team_loaded(team_id){
        return $(`.toggle_button[team_id = '${team_id}']`).attr("loaded");
    }

    function clear_field(team_id){
        $(`.field-container[team_id = '${team_id}'] > .basket-field`).empty();
    }

    function add_players(team_id){
        var temp_url = team_json_url.split('[team_id]').join(team_id);
        clear_field(team_id);
        $.getJSON( temp_url, function( data ) {
            var players = data.players;
            if(players === undefined) return;
            set_team_loaded(team_id, true);
            for(var i = 0; i < players.length; i++){
                add_player(players[i], team_id, i);
                //console.log(players[i]);
            }
        });
    }

    function add_player(player, team_id, i){
		var player_class = 'player-base';
		var temp_html_player = html_player.split('[data-id]').join(player.id);
		temp_html_player = temp_html_player.split('[data-amplua]').join(player.amplua);
		temp_html_player = temp_html_player.split('[t-shirt-url]').join(player.img);
		temp_html_player = temp_html_player.split('[club]').join(player.club);
		temp_html_player = temp_html_player.split('[player-name]').join(player.name);
		temp_html_player = temp_html_player.split('[points]').join(player.points);
        temp_html_player = temp_html_player.split('[top]').join(player_points[i].y + 2);
        temp_html_player = temp_html_player.split('[left]').join(player_points[i].x - 26);
		var $player = $(temp_html_player);
		$player.css('padding', '0');
			$(`.overBox[team_id = '${team_id}'] > .field-container > .basket-field`).each(function() {
				$(this).append($player);
			});
		}

function get_team_html(team_id){
	$.get(team_url.replace('[team_id]', team_id), function( data ) {
        set_team_loaded(team_id, true);
		var $team_field = parse_team_info(data);
		show_team_info(team_id, $team_field);
	});
}

function parse_team_info(team_html){
    var $team_html = $($($.parseHTML(team_html)).find('.overBox > .team-info-block'));
    return $team_html;
}

function show_team_info(team_id, $team_info){
    $(`.overBox[team_id = '${team_id}']`).append($team_info);
    $(`.overBox[team_id = '${team_id}'] > .team-info-block > .change-turnir`).remove();
    $(`.overBox[team_id = '${team_id}'] > .team-info-block > table:nth-child(1)`).remove();
    $(`.overBox[team_id = '${team_id}'] > .team-info-block > table`).addClass('sfg-profile-table');
    console.log($($team_info).html());
}
